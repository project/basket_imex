<?php

namespace Drupal\basket_imex\Plugin\IMEX\field;

/**
 * EmailField IMEX type.
 *
 * @BasketIMEXfield(
 *   id = "email",
 *   type = {"email"},
 *   name = "E-mail",
 * )
 */
class EmailField extends StringField {

}
