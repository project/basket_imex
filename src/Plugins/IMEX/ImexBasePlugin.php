<?php

namespace Drupal\basket_imex\Plugins\IMEX;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

class ImexBasePlugin extends BasketIMEXBaseForm {

  /**
   * Whether the field matching service is available to the plugin.
   */
  public function getImportExportSettings() {
    return TRUE;
  }

  /**
   * List of fields that will participate in the settings.
   */
  public function getSettingsFields() {
    return [
      'id',
      'title'
    ];
  }

  /**
   * Entity search.
   *
   * @param array $updateInfo
   *   Update data.
   */
  public function getEntity(array $updateInfo) {}

  /**
   * Call after updating / creating entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   * @param array $updateInfo
   *   Update data.
   */
  public function postSave(EntityInterface $entity, array $updateInfo) {}

  /**
   * Import fields.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState object.
   */
  public function getImportFormFields(array &$form, FormStateInterface $form_state) {}

  /**
   * Get the list to run for import.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState object.
   */
  public function getImportRows(FormStateInterface $form_state) {}

  /**
   * Import Handler Callback.
   *
   * @param array $info
   *   Info array.
   * @param array|null $context
   *   Context array.
   */
  public function processImport(array $info, ?array &$context) {}

  /**
   * Export fields.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState object.
   */
  public function getExportFormFields(array &$form, FormStateInterface $form_state) {}

  /**
   * Getting the list to run for export.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState object.
   */
  public function getExportRows(FormStateInterface $form_state) {}


  /**
   * Export Handler Callback.
   *
   * @param array $info
   *   Info array.
   * @param array|null $context
   *   Context array.
   */
  public function processExport(array $info, ?array &$context) {}

  /**
   * Imports/Exports multiple settings.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState object.
   * @param array $parents
   *   The parents of the form element.
   */
  public function imexMultiSettings(array &$form, FormStateInterface $form_state, array $parents) {}

  /**
   * Generate a multi file name.
   *
   * @param string $fileName
   *   The original file name.
   */
  public function imexMultiFileName($fileName) {
    return $fileName.'.txt';
  }

  /**
   * Alter the multi query.
   *
   * @param object $query
   *   The query object.
   * @param object $entity
   *   The entity object.
   */
  public function imexMultiQueryAlter(&$query, $entity) {
    $options = $entity->get('options');
    if(!empty($options['node_type'])) {
      $query->condition('n.type', $options['node_type']);
    }
  }

  /**
   * Get data for imexMulti.
   *
   * @param int $nid
   *   The node ID.
   * @param array $entityConfig
   *   The entity configuration.
   */
  public function imexMultiGetData($nid, $entityConfig) {
    return [];
  }

  /**
   * Saves multiple items to the specified entity.
   *
   * @param string $filePath
   *   The file path.
   * @param array $items
   *   The items to save.
   * @param object $entity
   *   The entity object to save the items to.
   */
  public function imexMultiSave($filePath, $items, $entity) {

  }
}
